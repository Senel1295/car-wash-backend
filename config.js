var mysql = require('mysql');

module.exports = {
    connection : function(){

        var config = {
            connectionLimit: 10,
            host     : 'localhost',
            port     : '3306',
            user     : 'root',
            password : '',
            database : 'rainy_day'
          }

        function addDisconnectHandler(connection) {
            connection.on("error", function (error) {
                if (error instanceof Error) {
                    if (error.code === "PROTOCOL_CONNECTION_LOST") {
                        console.error(error.stack);
                        console.log("Lost connection. Reconnecting...");
                            connection(connection.config);

                    } else if (error.fatal) {
                        // throw error;
                    }
                }
            });
        }

        var connection = mysql.createPool(config);
    
        // Add handlers.
        addDisconnectHandler(connection);
        return connection;
    }}