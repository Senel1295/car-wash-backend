var dao = require("./DbDao")
var helper  = require('./../controller/helper');
var sql = require('sqlstring');

module.exports.selectDeviceByDeviceId = function(deviceId, deviceType, callback){

    var query = sql.format("SELECT * FROM device WHERE device_id = ?;", [deviceId])
    dao.query(query, null, result => {
        callback(result);
    })
}

module.exports.insertOrUpdateDeviceByUser = function(user, callback){

    this.selectDeviceByDeviceId(user.device_id, user.device_type, result => {
        console.log('====selectDeviceByDeviceId==========');
        console.log(result);
        console.log('====================================');

        var deviceObj ={
            "device_id":user.device_id,
            "device_type":user.device_type,
            "token":user.token,
            "uid":user.id,
            "created_at":helper.getCurrentDate(),
            "updated_at":helper.getCurrentDate(),
            "is_active": 1
        }

        if(result === undefined || result == null || result.length == 0){
            this.insertDevice(deviceObj, result => {
                console.log('====insertDevice====================');
                console.log(result);
                console.log('====================================');
            })
        }else{
            console.log('====================================');
            console.log(deviceObj);
            console.log('====================================');
            this.updateDevice(deviceObj, result => {
                console.log('====updateDevice====================');
                console.log(result);
                console.log('====================================');
            })
        }
    })
}

module.exports.insertDevice = function(device, callback){

    var query = sql.format("INSERT INTO device SET ?", [device])

    dao.query(query, null, result => {
        callback(result);
    })
}

module.exports.updateDevice = function(device, callback){

    var query = sql.format(
        "UPDATE device SET token = ?, uid = ?, updated_at = ?, is_active = ? WHERE device.device_id = ? AND device.device_type = ?"
        , [device.token, device.uid, helper.getCurrentDate(), device.device_id, device.device_type, 1])

    dao.query(query, null, result => {
        callback(result);
    })
}

module.exports.getDevicesByPromotionId = function(pid, callback){
    var QUERY_SELECT_DEVICE = "SELECT d.id AS did, d.device_id, d.device_type, d.token, u.id AS user_id, l.location_id, p.promotion_id, p.promotion_name, p.promotion_desc, p.promotion_discount, p.promotion_image_url FROM promotion AS p"
        + " INNER JOIN location AS l ON p.lid = l.location_id"
        + " INNER JOIN user AS u ON u.lid = l.location_id"
        + " INNER JOIN device AS d ON d.uid = u.id"
        + " WHERE p.promotion_id = ?;"

    var query = sql.format(QUERY_SELECT_DEVICE, [pid])

    dao.query(query, null, result => {

        if(result === undefined || result === null || result.length === 0){
            callback(undefined);
        }else{
            callback(result);
        }
    })
}