var dao = require("./DbDao")
var helper  = require('./../controller/helper');
var sql = require('sqlstring');

module.exports.insertNotifications = function(notifications, callback){
    var query = sql.format("INSERT INTO notification (notification_time, pid, did, isRead, created_at, updated_at) VALUES ?", [ObjToArray(notifications)])

    dao.query(query, null, result => {

        if(result === undefined){
            callback(undefined);
        }else{
            callback(result);
        }

    })
}

module.exports.getNotifications = function(uid, callback){
    var QUERY_SELECT_NOTI = "SELECT n.notification_id, n.notification_time, n.pid AS promotion_id, n.isRead, p.promotion_name, p.promotion_desc, p.promotion_discount, p.promotion_image_url, l.location_name  FROM  user AS u"
        + " INNER JOIN device AS d ON u.id = d.uid"
        + " INNER JOIN notification AS n ON n.did = d.id"
        + " INNER JOIN promotion AS p ON n.pid = p.promotion_id"
        + " INNER JOIN location AS l ON p.lid = l.location_id"
        + " WHERE u.uid = ?"
        + " GROUP BY n.pid;"
    var query = sql.format(QUERY_SELECT_NOTI, [uid])

    dao.query(query, null, result => {

        if(result === undefined || result === null || result.length === 0){
            callback(undefined);
        }else{
            callback(result);
        }

    })
}

function ObjToArray(obj) {
    var arr = obj instanceof Array;
  
    return (arr ? obj : Object.keys(obj)).map(function(i) {
      var val = arr ? i : obj[i];
      if(typeof val === 'object')
        return ObjToArray(val);
      else
        return val;
    });
  }

