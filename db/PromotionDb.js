var dao = require("./DbDao")
var helper  = require('./../controller/helper');
var sql = require('sqlstring');

module.exports.insertPromotion = function(promotion, callback){
    var query = sql.format("INSERT INTO promotion SET ?", [promotion])

    dao.query(query, null, result => {

        if(result === undefined || result.insertId === undefined){
            callback(undefined);
        }else{
            callback(result);
        }
    })
}

module.exports.getPromotionsByUid = function(uid, callback){
    var QUERY_GET_PROMOTIONS = "SELECT l.location_id AS lid, l.location_name, p.promotion_id AS pid, p.promotion_name, p.promotion_desc AS description, p.promotion_discount AS discount, p.promotion_image_url AS image_url"
        + " FROM user AS u"
        + " INNER JOIN location AS l ON u.lid = l.location_id"
        + " INNER JOIN promotion AS p ON p.lid = l.location_id"
        + " WHERE u.uid = ?"
        + " GROUP BY p.promotion_id;"

    var query = sql.format(QUERY_GET_PROMOTIONS, [uid])

    dao.query(query, null, result => {

        console.log('====================================');
        console.log(result);
        console.log('====================================');

        if(result === undefined || result === null || result.length === 0){
            callback(undefined);
        }else{
            callback(result);
        }
    })
}