var dao = require("./DbDao")
var deviceDb = require("./DeviceDb")
var helper  = require('./../controller/helper');
var sql = require('sqlstring');

module.exports.selectUserByEmail = function(email, callback){
    var QUERY_SELECT_USER = "SELECT * FROM user AS u INNER JOIN location AS l ON l.location_id = u.lid WHERE u.email = ?;"
    dao.query(QUERY_SELECT_USER, email, result => {

        callback(result)
    })
}

module.exports.insertUser = function(user, callback){

    if(user.email === undefined || user.password === undefined || 
        user.user_type === undefined || user.lid === undefined || 
        user.device_type === undefined || user.token === undefined || user.device_id === undefined){
        callback("Required field(s) missing.", undefined);
        return
    }

    this.selectUserByEmail(user.email, result => {

        if(result.length === 0){

            var userObj ={
                "first_name":user.first_name,
                "last_name":user.last_name,
                "dob":user.dob,
                "address":user.address,
                "phone_number":user.phone_number,
                "email":user.email,
                "user_type": Number(user.user_type),
                "password":helper.encryptData(user.password),
                "image_url":user.image_url,
                "is_active": 1,
                "created_at":helper.getCurrentDate(),
                "updated_at":helper.getCurrentDate(),
                "uid": helper.encryptData(user.email),
                "lid": user.lid
            }

            dao.query("INSERT INTO user SET ?;", userObj, result => {
                if(result){
                    this.selectUserById(result.insertId, result => {
                        if(result === undefined || result === null || result.length == 0){
                            callback("Unknown error occurred", undefined)
                        }else{
                            callback(undefined, result[0])
                        }                        
                    });
                    
                    user.id = result.insertId
                    deviceDb.insertOrUpdateDeviceByUser(user, result =>{
                        console.log('====insertOrUpdateDeviceByUser======');
                        console.log(result);
                        console.log('====================================');
                    })
                }
            })
        } else {
            callback("This email is already exist.", undefined)
            return
        }
    });
}

module.exports.selectUserById = function(id, callback){
    var QUERY_SELECT_USER = "SELECT * FROM user AS u INNER JOIN location AS l ON l.location_id = u.lid WHERE u.id = ?;"
    dao.query(QUERY_SELECT_USER, id, result => {
        if(result === undefined ||  result === null || result.length == 0){
            callback(undefined)
        }else{
            result.id = undefined;
            callback(result)
        }
    })
}

module.exports.selectUserByLogin = function(user, callback){

    if(user.email === undefined || user.password === undefined || 
        user.device_type === undefined || user.token === undefined || user.device_id === undefined){
        callback("Required field(s) missing.", undefined);
        return
    }
    
    this.selectUserByEmail(user.email, result => {
        if(result === undefined || result === null || result.length == 0){
            callback("You entered username or password is wrong.", undefined)
        }else{

            if(result[0].password === helper.encryptData(user.password)){
                user.id = result[0].id
                deviceDb.insertOrUpdateDeviceByUser(user, result =>{
                    
                    console.log('====insertOrUpdateDeviceByUser======');
                    console.log(result);
                    console.log('====================================');
                })

                callback(undefined, result[0])
            }else{
                callback("You entered username or password is wrong.", undefined)
            }
        }
    })
}
