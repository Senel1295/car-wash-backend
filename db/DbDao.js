var conn = require("./../config")

module.exports.connection;

module.exports.query = function(query, params, callback) {
    conn.connection().getConnection(function(err, connection){
        this.connection = connection
        connection.query(query, params, (err, result) => {
            if (err){
                console.log('================ERROR===============');
                console.log(err);
                console.log('====================================');
                connection.destroy();
                return callback(null);
            }

            connection.destroy();
            return callback(result);

            });
    });
}