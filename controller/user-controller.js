var response = require('./response-util');
var db = require("./../db/UserDb")
var helper  = require('./helper');

module.exports.register=function(req, res){
    response.log("register",req.body);
    
    db.insertUser(req.body, (err, result) => {
        if(err){
            response.errorResponse(res, err)
            return
        }

        if(result){
            result.password = undefined
            result.is_active = undefined
            result.created_at = undefined
            result.updated_at = undefined
            result.lid = undefined
            result.id = undefined;

            response.dataResponse(res, "Success", result)
            return
        }
    })    
}

module.exports.login=function(req, res){
    response.log("login",req.body);

    db.selectUserByLogin(req.body, (err, result) => {
        if(err){
            response.errorResponse(res, err)
            return
        }

        if(result){
            result.password = undefined
            result.is_active = undefined
            result.created_at = undefined
            result.lid = undefined
            result.updated_at = undefined
            result.id = undefined;

            response.dataResponse(res, "Success", result)
            return
        }
    })
}

module.exports.logout=function(req, res){
    if(req === undefined || req === null || req.body.uid === undefined || req.body.uid === null || req.body.device_id === undefined || req.body.device_id === null){
        response.errorResponse(res, "Required field(s) missing.")
        return
    }

    response.log("logout", req.body);

    db.selectUserByUid(req.body.uid, req.body.device_id, (err, result) => {
        if(err){
            response.errorResponse(res, err)
            return
        }

        
        console.log('====================================');
        console.log(result);
        console.log('====================================');

        if(result === undefined || result === null || result.length === 0){
            response.errorResponse(res, "No devices found.")
        }else{
        }
    })
}

module.exports.signIn = function (req,res){
    response.log("signIn",req.body);
    sql.connection().getConnection(function(err, connection){

        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        signIn(connection, req, res)
    })
}

module.exports.getAllUsers = function (req,res){
    response.log("getAllUsers",req.body);
    sql.connection().getConnection(function(err, connection){

        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getAllUsers(connection, res)
    })
}

module.exports.updatePassword = function (req,res){
    response.log("updatePassword",req.body);
    sql.connection().getConnection(function(err, connection){

        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        updatePassword(connection, req, res)
    })
}
module.exports.updateInfo = function (req,res){
    response.log("updateInfo",req.body);
    sql.connection().getConnection(function(err, connection){

        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        updateInfo(connection, req, res)
    })
}

    function userRegister(conn, req, res){
        var userInsertQuery = 'INSERT INTO user set ?';
        if(!req || !req.body.first_name || !req.body.email || !req.body.phone_number || !req.body.address ){
            response.errorResponse(res, 'Missing required fields.');
            return
        }

        //encrypt the password for security
        var encryptedPw = helper.encryptData(req.body.password);
        console.log("encrypted pw id: ", encryptedPw);

        var user ={
            "first_name":req.body.first_name,
            "last_name":req.body.last_name,
            "dob":req.body.dob,
            "address":req.body.address,
            "phone_number":req.body.phone_number,
            "email":req.body.email,
            "user_type": 1,
            "password":encryptedPw,
            "image_url":req.body.image_url,
            "is_active": 1,
            "created_at":helper.getCurrentDate(),
            "updated_at":helper.getCurrentDate(),
            "uid": helper.uniqueUid(req.body.email) 
        }
        conn.query(userInsertQuery, user, function (error, results, fields ){
    
            if(error){
                console.log("error occured in registration ", error)
                response.errorResponse(res, 'This email is already registered.');
                response.log("userRegister", error)
                return
            }else{
                console.log("Successfully registered ")
                user.password = null
                response.dataResponse(res,"Successfully registered",user)
            }
         })
    }

    function signIn(conn, req, res){
        var userByEmailQuery = "SELECT * FROM user WHERE email  = ?";
    
        if(!req || !req.body || !req.body.email || !req.body.password){
            response.errorResponse(res, 'Missing required fields.');
            console.log("Missing required fields");
            if(conn)
                conn.destroy();
            return;
        }
        var user={
            "email":req.body.email,
            "password":req.body.password
        }
            conn.query(userByEmailQuery, user.email, function(error, results, fields){
                conn.destroy();
            if(error){
                response.errorResponse(res, "Internal server error!")
                console.log("Internal server error!", error);
                return;
            }
            if(results.length > 0 ){
                var sqlUser = results[0];
                if(helper.encryptData(user.password) === sqlUser.password){
                    sqlUser.password = null;
                    response.dataResponse(res, "User logging success.", sqlUser);
                    console.log("Successfully logged in ", sqlUser.email);
                }else{
                    response.errorResponse(res, 'Password or email is incorrect.');  
                    console.log("Password or email is incorrect.");
                }
            }else{
                response.errorResponse(res, 'No user found for the email address');
                console.log("No user found for the email address")
            }
        })
    }
    
    function updatePassword(conn, req, res){
        var updatePasswordQuery = "UPDATE user SET password = ? WHERE uid = ?";
        var selectUserPassowrdQuery = "SELECT password from user WHERE uid = ?";
        if(!req || !req.body || !req.body.password || !req.body.uid || !req.body.new_password){
            response.errorResponse(res, 'Missing required fields.');
            console.log("Missing required fields");
            if(conn)
                conn.destroy();
            return;
        }
        var user={
            "uid":req.body.uid,
            "password":req.body.password,
            "new_password":req.body.new_password
        }
            conn.query(selectUserPassowrdQuery, user.uid, function(error, results, fields){
            if(error){
                response.errorResponse(res, "Internal server error!")
                console.log("Internal server error!", error);
                return;
            }
            if(results.length > 0 ){
                var currentPassword = results[0];
                console.log("current password is: ", currentPassword.password)
                console.log("new encrypted password: ", helper.encryptData(user.password) )
                if(helper.encryptData(user.password) === currentPassword.password){
                    console.log("password match with current password ");
                    conn.query(updatePasswordQuery,[helper.encryptData(user.new_password),user.uid], function(error, results, fields){
                        if(!error){
                            response.successResponse(res,"Successfully updated password");
                            console.log("Successfully updated password");
                        }
                        else{
                            console.log(error);
                        }
                    })
                }else{
                    response.errorResponse(res, 'Current password is incorrect!');  
                    console.log("Current password is incorrect!");
                }
            }else{
                response.errorResponse(res, 'No users found for the uid');
                console.log("No users found for the uid")
            }
        })
    }

    function updateInfo(conn, req, res){
        var updateInfoQuery = "UPDATE user SET first_name = ?, email = ?, phone_number = ?, address = ?  WHERE uid = ? AND NOT EXISTS (SELECT email FROM user WHERE email = ?)";
        var selectUserPassowrdQuery = "SELECT password from user WHERE uid = ?";
        if(!req || !req.body || !req.body.password || !req.body.uid){
            response.errorResponse(res, 'Missing required fields.');
            console.log("Missing required fields");
            if(conn)
                conn.destroy();
            return;
        }
        var user={
            "uid":req.body.uid,
            "password":req.body.password,
            "first_name":req.body.first_name,
            "email":req.body.email,
            "phone":req.body.phone_number,
            "address":req.body.address
        }
            conn.query(selectUserPassowrdQuery, user.uid, function(error, results, fields){
            if(error){
                response.errorResponse(res, "Internal server error!")
                console.log("Internal server error!", error);
                return;
            }
            if(results.length > 0 ){
                var currentPassword = results[0];
                console.log("current password is: ", currentPassword.password)
                console.log("new encrypted password: ", helper.encryptData(user.password) )
                if(helper.encryptData(user.password) === currentPassword.password){
                    console.log("password match with current password ");

                    conn.query(updateInfoQuery,[user.first_name, user.email, user.phone, user.address, user.uid, user.email], function(error, results, fields){
                        if(!error){
                            response.dataResponse(res,"Successfully updated information",user);
                            console.log("Successfully updated information", user);
                        }
                        else{
                            console.log(error);
                            response.errorResponse(res, 'Email Already exists');
                        }
                    })
                }else{
                    response.errorResponse(res, 'password is incorrect!');  
                    console.log("password is incorrect!");
                }
            }else{
                response.errorResponse(res, 'No users found for the uid');
                console.log("No users found for the uid")
            }
        })
    }

    function getAllUsers(conn, res){
        var selectAllUser = "SELECT * FROM user WHERE is_active = ?";
        conn.query(selectAllUser, 1, function(error, results, fields){
            conn.destroy();
            if(error){
                response.errorResponse(res, "Internal server error!")
                console.log("Internal server error!", error);
                return
            }
            if(results.length>0){
                console.log("succesfully retrieved users", results);
                response.dataResponse(res, "success",results);
            }
            else{
                console.log("No users in the system");
                response.errorResponse(res, "No Users.");
            } 
        })
    }