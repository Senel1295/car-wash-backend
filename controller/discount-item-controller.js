var sql = require('./../config');
var SqlString = require('sqlstring');
var response = require('./response-util');
var helper  = require('./helper');

module.exports.assignDiscountItems=function(req, res){
    response.log("assignDiscountItems", req.body);

    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        assignDiscountItems(connection, req, res);
    })
}

module.exports.getPromotions=function(req, res){
    response.log("getPromotions");

    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getPromotions(connection, res);
    })
}

function assignDiscountItems(conn, req, res){
    var discountId;
    var itemId;

    response.log("assignDiscountItems", req.body)
    if(!req || !req.body.item_name){
        response.errorResponse(res, "Item name required")
        return
    } else if(!req.body.discount_name){
        response.errorResponse(res, "discount name required")
    }
    var selectItemId = "SELECT id FROM items WHERE item_name = ? ";
    conn.query(selectItemId, req.body.item_name, function(error, results, fields){
        if(results.length>0){
            itemId = results[0];
            console.log("itemId",itemId);

            var selectDiscountId = "SELECT id FROM discount WHERE discount_name = ? ";
            conn.query(selectDiscountId, req.body.discount_name, function(error, results, fields){
                if(results.length>0){
                    discountId = results[0];
                    console.log("discountId",discountId)

                    var discountItem ={
                        "item_id": itemId.id,
                        "discount_id": discountId.id,
                        "expiration_date": req.body.expiration_date,
                        "is_active": 1,
                        "created_at": helper.getCurrentDate(),
                        "updated_at": helper.getCurrentDate()
                    }
                    console.log("itemId.id",itemId.id);
                    console.log("discount.id",discountId.id);

                    var insertDiscountItems = "INSERT INTO discount_item set ? "
                    conn.query(insertDiscountItems, discountItem, function(error,results,fields){
                        conn.destroy();
                        if(error){
                            response.errorResponse(res, "Invalid information.");
                            response.log("assignDiscountItems", error)
                            return
                        }
                        else{
                            response.successResponse(res, "Successfully added!")
                        }
                    })
                }
            })
        }
    })
}

function getPromotions(conn, res){
    response.log("getPromotions")

    var selectAllPromotion = "SELECT * FROM ((discount_item INNER JOIN items ON discount_item.item_id = items.id)INNER JOIN discount ON discount_item.discount_id = discount.id)"
    conn.query(selectAllPromotion, function(error, results, fields){
        conn.destroy();
        if(error){
            response.errorResponse(res, "Internal server error!")
            return
        }
        if(results.length>0){
            console.log(results)
            response.dataResponse(res, "success", results);
        }
        else{
            response.errorResponse(res, "No Promotions");
        }
    })
}
