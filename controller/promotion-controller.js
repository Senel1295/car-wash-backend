var response = require('./response-util');
var db = require("./../db/PromotionDb");
var helper  = require('./helper');
var deviceCtrl  = require('./device-controller');

module.exports.addPromotion = function(req, res){
    response.log("addPromotion", req.file);
    response.log("addPromotion", req.body);

    
    if(req.file === undefined || req.body === undefined || req.body.promotion_name === undefined || req.body.lid === undefined 
        || req.body.promotion_discount === undefined){
        response.errorResponse(res, "Required field(s) missing.")
        return
    }

    var mime = require('mime-types')

    if(mime.extension(req.file.mimetype) === "jpeg" || mime.extension(req.file.mimetype) === "png"){
        var fs = require('fs');
        fs.rename(req.file.path, req.file.path + "." + mime.extension(req.file.mimetype))

        var promotionObj ={
            "promotion_name":req.body.promotion_name,
            "promotion_desc":req.body.promotion_desc,
            "promotion_discount":req.body.promotion_discount,
            "promotion_image_url":"promotion/" + req.file.filename + "." + mime.extension(req.file.mimetype),
            "created_at":helper.getCurrentDate(),
            "updated_at":helper.getCurrentDate(),
            "isActive": 1,
            "lid": req.body.lid
        }

        db.insertPromotion(promotionObj, result => {
            if(result === undefined){
                response.errorUnknown(res)
            }else{
                response.successResponse(res, "Success")
                deviceCtrl.sendNotification(result.insertId);
            }
        })
    }else{
        response.errorResponse(res, "Please upload valid type file jpeg/png")
        return
    }
}

module.exports.getPromotions = function(req, res){
    response.log("getPromotions", req.body);

    db.getPromotionsByUid(req.body.uid, result => {
        if(result === undefined){
            response.errorUnknown(res)
        }else{
            response.dataResponse(res, "Success", result)
        }
    })
}