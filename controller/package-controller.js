var sql = require('./../config');
var SqlString = require('sqlstring');
var response = require('./response-util');
var helper  = require('./helper');

module.exports.addPackage=function(req,res){
    response.log("addPackage", req.body);
    sql.connection().getConnection(function(err, connection){

        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        addPackage(connection, req, res)
    })
}

module.exports.getPackage=function(req,res){
    response.log("getPackage");
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getPackage(connection,req, res)
    })
}

module.exports.getAllPackage=function(req,res){
    response.log("getAllPackage", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getAllPackage(connection, res)
    })
}

module.exports.addItem=function(req,res){
    response.log("addItem", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        addItem(connection,req, res)
    })
}

module.exports.getAllItems=function(req,res){
    response.log("getAllItems", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getAllItems(connection, res)
    })
}

module.exports.getItems=function(req,res){
    response.log("getItems", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getItems(connection,req, res)
    })
}

function addPackage(conn, req, res){
    var serviceId;
    if(!req || !req.body.package_name){
        response.errorResponse(res, 'package name required.')
        console.log("package name required.")
        return;
    }else if (!req.body.service_name){
        response.errorResponse(res, 'service name required.')
        console.log("Service name required")
    }

    var selectQuery = "SELECT id FROM service WHERE service_name = ? ";
    conn.query(selectQuery, req.body.service_name,function(error, results, fields){
         if(results.length>0){
            serviceId = results[0];
         }
            package ={
                "package_name":req.body.package_name,
                "package_description":req.body.package_description,
                "service_id":serviceId.id,
                "image_url":req.body.image_url,
                "created_at":helper.getCurrentDate(),
                "updated_at":helper.getCurrentDate(),
                "is_active":1
            }

            var insertPackage = "INSERT INTO package set ? "
            console.log(package);
            conn.query(insertPackage , package, function(error, results, fields){
                conn.destroy();
                if(error){
                    response.errorResponse(res, 'Invalid package information.');
                    response.log("addPackage", error)
                    console.log("Invalid package information");
                    return
                }
                else{
                    response.successResponse(res, "Successfully added!")
                    console.log("Successfully added!");
                }
            })
    })
}

function getAllPackage(conn, res){
    var selectAllPackage = "SELECT * FROM package ";
    conn.query(selectAllPackage, function(error, results, fields){
        conn.destroy();
        if(error){
            response.errorResponse(res, "Internal server error!")
            return
        }
        if(results.length>0){
            console.log(results);
            response.dataResponse(res, "success",results);
        }
        else{
            response.errorResponse(res, "No Packages.");
        } 
    })
}

function getPackage(conn, req, res){
    response.log("getPackage", req.body)
    if(!req || !req.body.service_name){
        response.errorResponse(res, "Invalid service name")
        return
    }
    var selectPackageByService = "SELECT * FROM package INNER JOIN service ON package.service_id = service.id WHERE service_name = ? ";
    conn.query(selectPackageByService, req.body.service_name,function(error, results, fields){
        conn.destroy();
        if(error){
            response.errorResponse(res, "Internal server error!")
            return;
        }
        if(results.length > 0 ){
            var packages = results;
            response.dataResponse(res, "success", packages);
        } else {
            response.errorResponse(res, "No packages for service");
        }
    })
}

function addItem(conn, req, res){
    var packageId;
    response.log("addItem", req.body)
    if(!req || !req.body.item_name){
        response.errorResponse(res, "Item name required")
        return
    } else if(!req.body.package_name){
        response.errorResponse(res, "Package name required")
    }
    var selectPackageId = "SELECT id FROM package WHERE package_name = ? ";
    conn.query(selectPackageId, req.body.package_name, function(error, results, fields){
        if(results.length>0){
            packageId = results[0];
        }
        var item ={
            "item_name": req.body.item_name,
            "package_id": packageId.id,
            "item_description": req.body.item_description,
            "item_price": req.body.item_price,
            "image_url": req.body.image_url,
            "status": req.body.status,
            "currency": req.body.currency,
            "created_at":helper.getCurrentDate(),
            "updated_at":helper.getCurrentDate(),
            "is_active":1
        }
        console.log(item);
         conn.query('INSERT INTO items set ? ' , item, function(error, results, fields){
            conn.destroy();
            if(error){
                response.errorResponse(res, 'Invalid package information.');
                response.log("addItem", error)
                return
            }
            else{
                response.successResponse(res, "Successfully added!")
            }
        })

    })
    
}

function getAllItems(conn, res){
    var selectAllItems = "SELECT * FROM items "
    conn.query(selectAllItems, function(error, results, fields){
        conn.destroy();
        if(error){
            response.errorResponse(res, "Internal server error!")
            return
        }
        if(results.length>0){
            console.log(results);
            response.dataResponse(res, "success",results);
        }
        else{
            response.errorResponse(res, "No items.");
        } 
    })
}

function getItems(conn, req, res){
    response.log("getItems", req.body)
    if(!req || !req.body.package_name){
        response.errorResponse(res, "Package name required")
        return
    }
    var selectItemByPackage = "SELECT items.id, items.item_name, items.item_description, items.item_price, items.currency, items.status, items.image_url FROM items INNER JOIN package ON items.package_id = package.id WHERE package_name = ? ";
    conn.query(selectItemByPackage, req.body.package_name,function(error, results, fields){
        console.log(error)
        conn.destroy();
        if(error){
            response.errorResponse(res, "Internal server error!")
            return;
        }
        if(results.length > 0 ){
            var items = results;
            response.dataResponse(res, "success", items);
        }
    })
}

function getAllData(conn, req, res){
    response.log("getItems", req.body)
    if(!req || !req.body.package_name){
        response.errorResponse(res, "Package name required")
        return
    }
    var sqlQueryPackage = "SELECT * FROM items INNER JOIN package ON items.package_id = package.id WHERE package_name = ? ";
    conn.query(sqlQueryPackage, req.body.package_name,function(error, results, fields){
        if(error){
            response.errorResponse(res, "Internal server error!")
            return;
        }
        if(results.length > 0 ){
            var items = results;
            response.dataResponse(res, "success", items);
        }
    })
}
