
module.exports ={

    errorUnknown : function(res){
        var message = "Unknown error occurred."
        res.json({code:0, status:false, message:message});
    },
    
    errorResponse : function(res, errorMessage){
        res.json({code:0, status:false, message:errorMessage});
    },

    successResponse: function(res, successMessage){
        res.json({code:1, status:true, message:successMessage});
    },

    dataResponse: function(res, successMessage, data){
        res.json({code:1, status:true, message:successMessage, data:data});
    },

    log: function(title, message){
        console.log('================* ' + title + ' *====================');
        console.log(message);
        console.log('===================================================');
    }

}