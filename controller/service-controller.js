var sql = require('./../config');
var SqlString = require('sqlstring');
var response = require('./response-util');
var helper  = require('./helper');

module.exports.addService=function(req, res){
    response.log("addService",req.body);

    sql.connection().getConnection(function(err, connection){

        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        addService(connection, req, res)
    })
    
}

module.exports.getServices = function(req, res){
    sql.connection().getConnection(function(err, connection){

        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getServices(connection,res)
    });
}

function addService(conn, req, res){
    var insertServiceQuery = "INSERT INTO service set ?";
    if(!req || !req.body.service_name){
        console.log("Service name required.")
        response.errorResponse(res, 'Service name required.')
        return;
    }
    var service ={
        "service_name":req.body.service_name,
        "image_url":req.body.image_url,
        "created_at":helper.getCurrentDate(),
        "updated_at":helper.getCurrentDate(),
        "is_active": 1
    }
    conn.query(insertServiceQuery , service, function(error, results, fields){
        conn.destroy();
        if(error){
            console.log("Invalid service information ", error);
            response.errorResponse(res, 'Invalid service information.');
            return
        }
        else{
            console.log("Service added successfully")
            response.successResponse(res, "Successfully added!")
        }
    })

}

function getServices(conn, res){
    var selectAllFromService = "SELECT * FROM service ";
    conn.query(selectAllFromService, function(error, results, fields){
        conn.destroy();
    if(error){
        console.log("Internal server error ", error)
        response.errorResponse(res, "Internal server error!")
        return
    }
    if(results.length>0){
        console.log("services ", results);
        response.dataResponse(res, "",results);
    }
    else{
        console.log("No services in the system")
        response.errorResponse(res, "No services.");
    }
})
}