var sql = require('./../config');
var SqlString = require('sqlstring');
var response = require('./response-util');
var helper  = require('./helper');

module.exports.booking=function(req, res){
    response.log("Booking", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        booking(connection, req, res);
    })
}

module.exports.getAllAppointments=function(req, res){
    response.log("getAllAppointments", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getAllAppointments(connection, res);
    })
}

module.exports.acceptBooking=function(req, res){
    response.log("acceptBooking", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server errror!")
            return;
        }
        acceptBooking(connection, req, res);
    })
}

module.exports.getAllBookingsForId=function(req, res){
    response.log("getallBookingsForUid", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return;
        }
        getAllBookingsForId(connection,req,res);
    })
}

module.exports.getBookingByTag=function(req, res){
    response.log("getBookingByTag", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return;
        }
        getBookingByTag(connection,req,res);
    })
}

module.exports.getAllAppointmentsByUid=function(req, res){
    response.log("getAllAppointmentsByUid", req.body);

    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getAllAppointmentsByUid(connection, req, res)
    })
}

function booking(conn, req, res){
    var bulk = []; 
    var bulkInsert;
    var bookingId;
    var userId;

    response.log("Booking ", req.body);

    if(!req || !req.body.item_id){
        console.log("Missing required fields")
        response.errorResponse(res, 'Missing required fields.')
        if(conn)
            conn.destroy();
            return;
    }
    var selectUidQuery = "SELECT uid FROM user WHERE uid = ?";
    conn.query(selectUidQuery, req.body.uid, function(error, results, fields){
        if(results.length>0){
            userId = results[0]
            console.log("Booking receipt will be generated to user.... ", userId);
            const generateID = require('node-unique-id-generator');
            bookingId = generateID.generateUniqueId('BK-');
            console.log("Booking ID ", bookingId);
            var total = 0;
                var booking_items ={
                    "uid":req.body.uid,
                    "booking_id":bookingId,
                    "datetime":req.body.datetime,
                    "total": total,
                    "status":1,
                    "is_active":1,
                    "created_at":helper.getCurrentDate(),
                    "updated_at":helper.getCurrentDate(),
                }
                varInsertBookingId = "INSERT INTO booking_items set ? ";
                selectIdQuery = "SELECT id FROM booking_items WHERE booking_id = ?"
                var id;
                conn.query(varInsertBookingId, booking_items, function(error, results, fields){
                    if(!error){
                        console.error("phase 1 success")
                        conn.query(selectIdQuery, booking_items.booking_id, function(error, results, fields){
                            if(results.length>0){
                                id = results[0].id;
                                for(var i = 0; i < req.body.item_id.length;i++){
                                    var booking ={
                                        "booking_id":id,
                                        "item_id":req.body.item_id[i],
                                        "created_at":helper.getCurrentDate(),
                                        "updated_at":helper.getCurrentDate(),
                                        "is_active":1
                                    }
                                    console.log("creating bulk booking data....");
                                    bulk.push(booking);
                                }
                    
                                console.log("bulk data ",bulk);
                                bulkInsert = ObjToArray(bulk)
                                console.log("bulk insert ", bulkInsert)
                                var bulkInsertQuery = "INSERT INTO booking (booking_id, item_id, created_at,updated_at,is_active) VALUES ?"
                                conn.query(bulkInsertQuery, [bulkInsert], function (error, results, fields ){
                                    conn.destroy();
                                    if(error){
                                        response.errorResponse(res, 'Invalid booking.');
                                        response.log("booking", error)
                                        return
                                    }else{
                                        response.successResponse(res, "Booking successful!")
                                    }
                                })
                                
                            }
                            else{
                                console.log(error);
                            }
                        })
                    }
                    else{
                        console.log(error)
                        console.log("Error in inserting");
                        response.errorResponse(res, 'Internal server error');
                    }
                })
        } else {
            conn.destroy();
            console.log("User ID doesn't match with our registered users");
            response.errorResponse(res, "User ID doesn't match with our registered users");
        }
    })
}

function getAllAppointments(conn, res){
    var selectBookingID = "SELECT DISTINCT booking_id FROM booking";
    conn.query(selectBookingID, function(error, results, fields){
        conn.destroy();
        if(error){
            console.log("Internal server error!")
            response.errorResponse(res, "Internal server error!")
            return
        }
        if(results.length>0){
            console.log("All Bookings ", results);
            response.dataResponse(res, "success", results);
        }
        else{
            console.log("Currently you have no bookings.");
            response.errorResponse(res, "Currently you have no bookings.");
        }
    })
}

function getAllAppointmentsByUid(conn, req, res){
    response.log("getAllAppointmentsByUid", req.body)
    if(!req||!req.body.uid||!req.body.tag){
        response.errorResponse(res, "Missing required fields.")
        if(conn)
        conn.destroy();
        return
    }
    var selectUpcomingBooking = "SELECT DISTINCT booking_id FROM booking WHERE uid = ? AND booking.status != ? AND booking.datetime >= ?";
    var selectHistoryBooking = "SELECT DISTINCT booking_id FROM booking WHERE uid = ? AND booking.datetime < ?";
    var executeQuery;
    var dataArray = [];
    if(req.body.tag == "UPCOMING"){
        console.log("UPCOMING SELECTED")
        executeQuery = selectUpcomingBooking;
        dataArray = [req.body.uid,2,helper.getCurrentDate()]
    }
    else{
        console.log("HISTORY SELECTED")
        executeQuery = selectHistoryBooking;
        dataArray = [req.body.uid, helper.getCurrentDate()]
    }
    var selectBookingID = "";
    conn.query(executeQuery, dataArray, function(error, results, fields){
        conn.destroy();
        if(error){
            console.log("Internal server error!")
            response.errorResponse(res, "Internal server error!")
            return
        }
        if(results.length>0){
            console.log("All Bookings ", results);
            response.dataResponse(res, "success", results);
        }
        else{
            console.log("Currently you have no bookings.");
            response.errorResponse(res, "Currently you have no bookings.");
        }
    })
}

function acceptBooking(conn, req, res){
    response.log("acceptBooking", req.body)
    if(!req || !req.body.booking_id){
        response.errorResponse(res, "Booking ID required")
        return
    } else if(!req.body.status){
        response.errorResponse(res, "Booking status required")
    }
    var updateBooking = "UPDATE booking SET status = "+req.body.status +" WHERE booking_id = ? ";
    conn.query(updateBooking, req.body.booking_id, function(error,results,fields){
        conn.destroy();
        if(error){
            console.log("Status update failed", error)
            response.errorResponse(res, "Status update failed.");
            return
        }
        else{
            response.successResponse(res, "Booking approved!");
            console.log("Successfully approved booking")
        }
    })
}

function getAllBookingsForId(conn, req, res){
    var bookingsForId;
    response.log("getAllBookingsForUid", req.body);
    if(!req || !req.body.booking_id){
        response.errorResponse(res, "Missing required fields")
        console.log("Missing required fields")
        if(conn)
        conn.destroy();
        return;
    }
    var selectAllBookingsById =  "SELECT items.item_name, items.item_description, items.item_price, items.image_url, items.status, items.currency, items.package_id FROM items INNER JOIN booking ON booking.item_id = items.id INNER JOIN booking_items ON booking.booking_id = booking_items.id  WHERE booking_items.booking_id = ? AND booking_items.is_active = ? AND items.is_active = ?";
    conn.query(selectAllBookingsById, [req.body.booking_id,1,1], function(error, results, fields){
        conn.destroy();
        if(error){
            console.log(error)
            console.log("Internal server error");
            response.errorResponse(res, "Internal server error!")
            return;
        } else if(results.length > 0){
            bookingsForId = results;
            console.log("Bookings: ", bookingsForId)
            response.dataResponse(res, "Success", bookingsForId);
            console.log("Successfully retrieved all Bookings for ID");
        }
    })
}

function getBookingByTag(conn, req, res){
    var bookingsForUid;
    response.log("getBookingByTag", req.body);
    if(!req || !req.body.uid || !req.body.tag){
        response.errorResponse(res, "Missing required fields")
        console.log("Missing required fields")
        if(conn)
        conn.destroy();
        return;
    }
    var selectUpcomingBooking = "SELECT * FROM booking_items WHERE uid = ? AND status != ? AND datetime >= ?";
    var selectHistoryBooking = "SELECT * FROM booking_items WHERE uid = ?  AND datetime < ?";
    var executeQuery;
    var dataArray = [];
    if(req.body.tag == "UPCOMING"){
        console.log("UPCOMING SELECTED")
        executeQuery = selectUpcomingBooking;
        dataArray = [req.body.uid,2,helper.getCurrentDate()]
    }
    else{
        console.log("HISTORY SELECTED")
        executeQuery = selectHistoryBooking;
        dataArray = [req.body.uid, helper.getCurrentDate()]
    }
    conn.query(executeQuery, dataArray, function(error, results, fields){
        conn.destroy();
        if(error){
            console.log(error)
            console.log("Internal server error");
            response.errorResponse(res, "Internal server error!")
            return;
        } else if(results.length > 0){
            bookingsForUid = results;
            console.log(bookingsForUid)
            response.dataResponse(res, "Success", bookingsForUid);
            console.log("Successfully retrieved all Bookings for UID");
        }
    })
}

function ObjToArray(obj) {
    var arr = obj instanceof Array;
  
    return (arr ? obj : Object.keys(obj)).map(function(i) {
      var val = arr ? i : obj[i];
      if(typeof val === 'object')
        return ObjToArray(val);
      else
        return val;
    });
  }
   
