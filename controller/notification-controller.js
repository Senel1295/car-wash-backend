// var sql = require('./../config');
// var SqlString = require('sqlstring');
var response = require('./response-util');
var db = require("./../db/Notificationdb")

module.exports.getNotifications=function(req, res){
    response.log("getNotifications", req.body);
    
    db.getNotifications(req.body.uid, result => {
        if(!result){
            response.errorUnknown(res)
            return
        }

        response.dataResponse(res, "Success", result)
        return
    });
}