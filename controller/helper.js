var sql = require('./../config');
module.exports={

    getCurrentDate : function(){
        return Math.round((new Date()).getTime() / 1000);
    },

    encryptData : function(data){
        try {
            var crypto = require('crypto');
            var encryptedPw = crypto.createHash('md5').update(data).digest("hex");
            return encryptedPw;
        } catch (error) {
            return data;
        }
    }
}