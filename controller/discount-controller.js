var sql = require('./../config');
var SqlString = require('sqlstring');
var response = require('./response-util');
var helper  = require('./helper');

module.exports.addDiscount=function(req, res){
    response.log("addToCart", req.body);

    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        addDiscount(connection, req, res)
    })
}

module.exports.removeDiscount=function(req, res){
    response.log("removeDiscount", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        removeDiscount(connection, req, res)
    })
}

module.exports.getAllDiscounts=function(req, res){
    response.log("getAllDiscounts", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getAllDiscounts(connection, res)
    })
}

function addDiscount(conn, req, res){
    response.log("addDiscount", req.body);
    if(!req){
        response.errorResponse(res, "Missing required fields")
        if(conn)
        conn.destroy();
        return;
    }
    discount ={
        "discount":req.body.discount,
        "discount_name":req.body.discount_name,
        "is_percentage":req.body.is_percentage,
        "description":req.body.description,
        "promo_image":req.body.promo_image,
        "created_at":helper.getCurrentDate(),
        "updated_at":helper.getCurrentDate(),
        "is_active": 1
        }
        var insertDiscount = "INSERT INTO discount set ? "
        console.log(discount);
        conn.query(insertDiscount, discount, function(error, results, fields){
            conn.destroy();
        if(error){
            response.errorResponse(res, 'Invalid package information.');
            response.log("addPackage", error)
            return
        }
        else{
            response.successResponse(res, "Successfully added")
        }

    })
}

function removeDiscount(conn, req, res){
    response.log("removeDiscount", req.body);
    if(!req){
        response.errorResponse(res, "Invalid remove discount")
        if(conn)
        conn.destroy();
        return;
    }
    var deleteDiscount = "UPDATE discount SET is_active = 1 WHERE id = ? "
    conn.query(deleteDiscount, req.body.id, function(error,results,fields){
        conn.destroy();
        if(error){
            response.errorResponse(res, "discount remove failed.");
            response.log("deleteDiscount", error)
            return
        }
        else{
            response.successResponse(res, "Successfully deleted!");
        }
    })

}

function getAllDiscounts(conn, res){
    var selectAllDiscounts = "SELECT * FROM discount";
    conn.query(selectAllDiscounts, function(error, results, fields){
        conn.destroy();
        if(error){
            response.errorResponse(res, "Internal server error!")
            return
        }
        if(results.length>0){
            console.log(results);
            response.dataResponse(res, "success",results);
        }
        else{
            response.errorResponse(res, "No discounts");
        }
    })
}