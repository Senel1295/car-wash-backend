var admin = require('firebase-admin');
var helper  = require('./helper');
var notiDb = require('./../db/Notificationdb');

var serviceAccount = require('./../private_key.json');
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://rainy-day-27071.firebaseio.com'
});

// See documentation on defining a message payload.
var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)    
    notification: {
        title: "", 
        body: "" 
    },
    
    data: {
        location_id: "", 
        promotion_id: "",
        promotion_name: "",
        promotion_desc:"",
        promotion_discount:"",
        promotion_image_url:""
    }
};

// {  //you can send only notification or only data(or include both)
//     my_key: 'my value',
//     my_another_key: 'my another value'
// }

// Send a message to the device corresponding to the provided
// registration token.

module.exports.sendNotifications = function(data){

    // This registration token comes from the client FCM SDKs.
    var registrationTokens = [];
    
    var notifications = []

    if(data === undefined || data === null || data.length === 0){
        console.log("sendNotifications ==> ERROR");
        return
    }

    for (index = 0; index < data.length; ++index) {

        var notification = {
            "notification_time": helper.getCurrentDate(),
            "pid": "",
            "did": "",
            "isRead": 0,
            "created_at": helper.getCurrentDate(),
            "updated_at": helper.getCurrentDate()
        };

        notification.pid = data[index].promotion_id
        notification.did = data[index].did
        notifications.push(notification)

        registrationTokens.push(data[index].token);
        message.notification.title = data[index].promotion_name
        message.notification.body = "You have " + data[index].promotion_discount +  " of discount."
    }

    message.data.location_id = data[0].location_id + "";
    message.data.promotion_id = data[0].promotion_id + "";
    message.data.promotion_name = data[0].promotion_name;
    message.data.promotion_desc = data[0].promotion_desc;
    message.data.promotion_discount = data[0].promotion_discount;
    message.data.promotion_image_url = data[0].promotion_image_url;

    console.log('====================================');
    console.log(message);
    console.log('====================================');

    admin.messaging().sendToDevice(registrationTokens, message).then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent message:', response);
    })
    .catch((error) => {
        console.log('Error sending message:', error);
    });

    notiDb.insertNotifications(notifications, result => {
        console.log('====insertNotifications=============');
        console.log(result);
        console.log('====================================');
    })
}