// var sql = require('./../config');
// var SqlString = require('sqlstring');
var response = require('./response-util');
var db = require("./../db/LocationDb")

module.exports.getLocations=function(req, res){
    response.log("getLocations", "");
    
    db.getLocations(result => {
        if(!result){
            response.errorUnknown(res)
            return
        }

        response.dataResponse(res, "Success", result)
        return
    });
}