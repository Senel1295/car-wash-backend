var db = require("./../db/DeviceDb");
var helper  = require('./helper');
var fcm = require("./fcm-controller");

module.exports.sendNotification = function(pid){
    console.log('====sendNotification================');
    console.log(pid);
    console.log('====================================');

    db.getDevicesByPromotionId(pid, function(result){
        console.log('====getDevicesByPromotionId=========');
        console.log(result);
        console.log('====================================');

        fcm.sendNotifications(result)
    })
}