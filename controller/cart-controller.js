var sql = require('./../config');
var SqlString = require('sqlstring');
var response = require('./response-util');
var helper  = require('./helper');

module.exports.addToCart=function(req, res){
    response.log("addToCart", req.body);

    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        addToCart(connection, req, res)
    })
}

module.exports.removeFromCart=function(req, res){
    response.log("removeFromCart", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        removeFromCart(connection, req, res)
    })
}

module.exports.getCart=function(req, res){
    response.log("getCart", req.body);
    sql.connection().getConnection(function(err, connection){
        if(!connection){
            console.log(err);
            response.errorResponse(res, "Internal server error!")
            return
        }
        getCart(connection, req, res)
    })
}

function addToCart(conn, req, res){
    response.log("addToCart", req.body);
    if(!req){
        response.errorResponse(res, "Missing required fields.")
        if(conn)
        conn.destroy();
        return;
    }
    var bulk = [];
    var bulkInsert;

    for(var i = 0; i < req.body.item_id.length;i++){
        var cart ={
            "uid":req.body.uid,
            "item_id":req.body.item_id[i],
            "created_at":helper.getCurrentDate(),
            "updated_at":helper.getCurrentDate(),
            "is_active":1,
        }
    bulk.push(cart);
    }
    console.log("BULK");
    console.log(bulk)
    bulkInsert = ObjToArray(bulk)
    console.log(bulkInsert)
    conn.query("INSERT INTO cart (uid,item_id,created_at,updated_at,is_active) VALUES ?", [bulkInsert], function (error, results, fields ){
        conn.destroy();
        if(error){
            response.errorResponse(res, 'Invalid cart information.');
            response.log("booking", error)
            return
        }else{
            response.successResponse(res, "Successfully added to cart!")
        }
    })

}

function removeFromCart(conn, req, res){
    response.log("removeFromCart", req.body);
    if(!req){
        response.errorResponse(res, "Invalid remove item")
        if(conn)
        conn.destroy();
        return;
    }
    var deleteItemQuery = "UPDATE cart SET is_active = 0 WHERE item_id = ? "
    conn.query(deleteItemQuery, req.body.item_id, function(error,results,fields){
        conn.destroy();
        if(error){
            response.errorResponse(res, "cart remove failed.");
            response.log("deleteItemQuery", error)
            return
        }
        else{
            response.successResponse(res, "Successfully deleted!");
        }
    })

}

function getCart(conn, req, res){
    response.log("getCart", req.body)
    if(!req || !req.body.uid){
        response.errorResponse(res, "Invalid user")
        return
    }
    var selectAllCartByUid = "SELECT * FROM cart INNER JOIN items ON cart.item_id = items.id WHERE uid = ? ";
    conn.query(selectAllCartByUid, req.body.uid,function(error, results, fields){
        conn.destroy();
        if(error){
            console.log(error)
            response.errorResponse(res, "Internal server error!")
            return;
        }
        if(results.length > 0 ){
            var cart = results;
            response.dataResponse(res, "success", cart);
        } else {
            response.errorResponse(res, "Cart empty");
        }
    })
}

function ObjToArray(obj) {
    var arr = obj instanceof Array;
  
    return (arr ? obj : Object.keys(obj)).map(function(i) {
      var val = arr ? i : obj[i];
      if(typeof val === 'object')
        return ObjToArray(val);
      else
        return val;
    });
  }
   