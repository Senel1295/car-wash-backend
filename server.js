var express=require("express");
var bodyParser=require('body-parser');

var multer  = require('multer')
var upload = multer({ dest: 'uploads/' })

var app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use('/promotion', express.static('uploads'))

var user = require('./controller/user-controller')
var location = require('./controller/location-controller')
var promotion = require('./controller/promotion-controller')
var notification = require('./controller/notification-controller')
var package = require('./controller/package-controller')
var booking = require('./controller/booking-controller')
var cart = require('./controller/cart-controller')
var discount = require('./controller/discount-controller')
var discount_item = require('./controller/discount-item-controller')

app.post('/api/login', user.login)
app.post('/api/register', user.register)
app.post('/api/logout', user.logout)
app.get('/api/getLocations', location.getLocations)
app.post('/api/addPromotion', upload.single('image'), promotion.addPromotion)
app.post('/api/getPromotions', promotion.getPromotions)
app.post('/api/getNotifications', notification.getNotifications)

// app.post('/api/signIn', user.signIn)
// app.post('/api/addService', service.addService)
// app.get('/api/getService', service.getServices)
// app.post('/api/addPackage', package.addPackage )
// app.get('/api/getAllPackage', package.getAllPackage)
// app.post('/api/getPackage', package.getPackage)
// app.post('/api/addItem', package.addItem)
// app.get('/api/getAllItems', package.getAllItems)
// app.post('/api/getItems', package.getItems)
// app.post('/api/booking', booking.booking)
// app.get('/api/getAllAppointments', booking.getAllAppointments);
// app.post('/api/acceptBooking', booking.acceptBooking)
// app.post('/api/addToCart', cart.addToCart)
// app.post('/api/removeFromCart', cart.removeFromCart)
// app.post('/api/addDiscount', discount.addDiscount)
// app.post('/api/removeDiscount', discount.removeDiscount)
// app.get('/api/getAllUsers', user.getAllUsers)
// app.get('/api/getAllPromotions', discount.getAllDiscounts)
// app.post('/api/assignDiscountItems', discount_item.assignDiscountItems)
// app.get('/api/getPromotions', discount_item.getPromotions)
// app.post('/api/getAllBookingsForId', booking.getAllBookingsForId)
// app.post('/api/getBookingByTag', booking.getBookingByTag)
// app.post('/api/updatePassword', user.updatePassword)
// app.post('/api/updateInfo', user.updateInfo)
// app.post('/api/getCart', cart.getCart)
// app.post('/api/getAllAppointmentsByUid', booking.getAllAppointmentsByUid)



app.listen(8070);

console.log('====================================');
console.log("rainy-day-api listen on port 8070");
console.log('====================================');
